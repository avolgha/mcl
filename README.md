# MCL

> Little command line tool for automation

## Compile by yourself

```bash
# I use yarnpkg as my package manager so
# these commands will be written not for npm

$ yarn package
$ node .\bin\main.js
```
