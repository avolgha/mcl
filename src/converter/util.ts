import ffmpeg from "fluent-ffmpeg";
import { defaultLogger } from "../logger.js";

/**
 * ffmpeg options to convert a mp4-file to webm format
 */
export const mp4ToWebmOptions = [
  "-c:v",
  "libvpx",
  "-pix_fmt",
  "yuv420p",
  "-c:a",
  "libvorbis",
  "-quality",
  "good",
  "-b:v",
  "2M",
  "-crf",
  "5",
  "-f",
  "webm",
];

/**
 * Convert an input file with the help of ffmpeg to the specified target type
 *
 * @param input The input file path
 * @param output The output file path
 * @param options The ffmpeg options
 * @returns Returns a promise that resolves if ffmpeg is finished
 */
export function convert(
  input: string,
  output: string,
  options: string[]
): Promise<void> {
  return new Promise((resolve, reject) => {
    var ffm = ffmpeg(input).outputOptions(options);
    ffm.on("start", (commandLine) => {
      defaultLogger.debug("spawned FFMPEG with command: " + commandLine);
    });
    ffm.output(output);
    ffm.on("error", (error, _, stderr) => {
      error.stderr = stderr;
      reject(error);
    });
    ffm.run();
    if (typeof output === "string") {
      ffm.on("end", function () {
        resolve();
      });
    } else {
      resolve();
    }
    return ffm;
  });
}
