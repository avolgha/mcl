import fs from "fs";
import { defaultLogger } from "../logger.js";
import { convert, mp4ToWebmOptions } from "./util.js";

/**
 * Converts a mp4-file to webm format
 *
 * @param input The input file path
 * @param output The output file path
 * @returns Returns a promise with the information if ffmpeg and the process were successfull
 */
export default async function convertMp4ToWebm(input: string, output: string) {
  defaultLogger.debug("converting file '{s}' from mp4 to webm", input);
  if (!fs.existsSync(input)) {
    return defaultLogger.error(
      "given input file '{s}' does not exists.",
      input
    );
  }
  if (!fs.statSync(input).isFile()) {
    return defaultLogger.error("given input file '{s}' is not a file.", input);
  }

  if (fs.existsSync(output)) {
    return defaultLogger.error(
      "output file '{s}' does already exists on filesystem.",
      output
    );
  }

  let err;
  await convert(input, output, mp4ToWebmOptions)
    .catch((error) => {
      err = error;
    })
    .then((_) => defaultLogger.debug("finished convertion"));

  if (err) {
    return defaultLogger.error(`${err}`);
  }
  return true;
}
