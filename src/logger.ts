import { fmt, colors } from "nstd";

/**
 * Interface for defining how the logger should look like
 */
export default interface Logger {
  /**
   * Print a message to the specificed stream
   *
   * @param message The message you want to print
   * @param stream The stream you want to print to. Default is stdout
   */
  _print(message: string, stream?: NodeJS.WritableStream): void;

  /**
   * Print an info message to stdout
   *
   * @param message The message you want to print
   * @param objects Objects you want to format the message with
   */
  info(message: string, ...objects: any): void;

  /**
   * Print a debug message to stdout.
   * Only possible if verbose output was enabled
   *
   * @param message The message you want to print
   * @param objects Objects you want to format the message with
   */
  debug(message: string, ...objects: any): void;

  /**
   * Print a warn message to stdout
   *
   * @param message The message you want to print
   * @param objects Objects you want to format the message with
   */
  warn(message: string, ...objects: any): void;

  /**
   * Print an error message to stderr
   *
   * @param message The message you want to print
   * @param objects Objects you want to format the message with
   *
   * @returns Returns false to easy handle return statements with error log
   */
  error(message: string, ...objects: any): boolean;
}

let verboseOutput = false;

/**
 * Toggle the state if the logger should print debug / verbose output
 *
 * @param state The new state of the verbose output
 */
export function setVerboseOutput(state: boolean) {
  verboseOutput = state;
}

/**
 * The default logger implementation for MCL
 */
export const defaultLogger: Logger = {
  _print: function (
    message: string,
    stream: NodeJS.WritableStream = process.stdout
  ): void {
    stream.write(`${message}\n`);
  },

  info: function (message: string, ...objects: any): void {
    this._print(
      `${colors.bgGreen("info")}${colors.white(": " + fmt(message, objects))}`
    );
  },

  debug: function (message: string, ...objects: any): void {
    verboseOutput &&
      this._print(
        `${colors.bgCyan("debug")}${colors.white(": " + fmt(message, objects))}`
      );
  },

  warn: function (message: string, ...objects: any): void {
    this._print(
      `${colors.bgYellow("warn")}${colors.white(": " + fmt(message, objects))}`
    );
  },

  error: function (message: string, ...objects: any): boolean {
    this._print(
      `${colors.bgRed("error")}${colors.white(": " + fmt(message, objects))}`,
      process.stderr
    );
    return false;
  },
};
