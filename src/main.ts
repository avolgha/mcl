#!/usr/bin/env node
import dotenv from "dotenv";
dotenv.config();
import { Command } from "commander";
import fs from "fs";
import pkg from "inquirer";
const { prompt } = pkg;
import * as npath from "path";
import { mp4ToWebm } from "./converters.js";
import { defaultLogger, setVerboseOutput } from "./logger.js";
import { templates as templateCollection } from "./templates.js";
import { getPackageJson, installDependencies } from "./util.js";

const cli = new Command();
const packageJson = getPackageJson();

cli.name(packageJson.name);
cli.description(packageJson.description);
cli.version(packageJson.version);

cli
  .command("list-converters")
  .description("list all available converters")
  .action(() => defaultLogger.info(`Available: ${["mp4-webm"].join(", ")}`));

cli
  .command("convert")
  .description("converts a given input file into another type")
  .argument("<conversion>", "the conversion that should happen")
  .argument("[input]", "the input file")
  .argument("[output]", "the output file")
  .option("-v, --verbose", "set verbose output", false)
  .action(
    async (
      method: string,
      input: string | undefined,
      output: string | undefined,
      options: { verbose: boolean }
    ) => {
      setVerboseOutput(options.verbose);

      if (input === undefined) {
        const result = await prompt({
          type: "input",
          name: "inputFile",
          message: "please enter the path of the input file",
          validate: (input) => input !== "",
        });
        input = result.inputFile;
      }

      if (output === undefined) {
        const defaults: {
          [ext: string]: string;
        } = {
          ".mp4": ".webm",
        };

        const result = await prompt({
          type: "input",
          name: "outputFile",
          message: "please enter the path of the output file",
          default: (() => {
            const index = input!.indexOf(".");
            const ext = input!.substring(index);
            const defaultExt = defaults[ext];
            if (defaultExt) {
              return `${input!.substring(0, index)}${defaultExt}`;
            }
            return "";
          })(),
          validate: (input) => input !== "",
        });

        output = result.outputFile;
      }

      defaultLogger.info("looking for a conversion method...");

      let fn: (source: string, output: string) => Promise<boolean>;

      switch (method) {
        case "mp4-webm":
          fn = mp4ToWebm;
          break;
        default:
          defaultLogger.error(
            "cannot find a converter with the name '{s}'.",
            method
          );
          return;
      }

      defaultLogger.info("starting conversion process...");

      const result = await fn(input!, output!);

      result &&
        defaultLogger.info("finished! output was written to '{s}'.", output);

      process.exit(result ? 0 : 1);
    }
  );

cli
  .command("generate")
  .description("generates a random project for you")
  .argument("[name]", "the name of the template")
  .option("-y, --yes", "skip all questions with yes", false)
  .option("--use-npm", "should not ask for yarnpkg as package manager", false)
  .option("--use-yarn", "should not ask for npm as package manager", true)
  .option("--no-features", "disable the prompt for features", false)
  .option("-v, --verbose", "set verbose output", false)
  .action(
    async (
      name: string | undefined,
      options: {
        yes: boolean;
        useNpm: boolean;
        useYarn: boolean;
        noFeatures: boolean;
        verbose: boolean;
      }
    ) => {
      if (options.useNpm === true && options.useYarn === true) {
        defaultLogger.error(
          "cannot have useNpm and useYarn flag set together."
        );
        return;
      }

      setVerboseOutput(options.verbose);

      if (name === undefined) {
        const templates = [];
        for (const name in templateCollection) {
          templates.push(name);
        }

        const result = await prompt({
          type: "list",
          name: "templateName",
          message: "please choose a template from the list",
          choices: templates.map((template) => {
            return {
              name: template,
              value: template,
              type: "choice",
            };
          }),
        });

        name = result.templateName;
      }

      const template = templateCollection[name!];

      if (template === undefined) {
        defaultLogger.error("unknown template '{s}'.", name);
        return;
      }

      const { cwd, project } = await prompt([
        {
          type: "input",
          name: "cwd",
          message: "where do you want to create the project in",
          validate: (input) => {
            if (input === "") {
              return false;
            }

            return fs.existsSync(input) && fs.statSync(input).isDirectory();
          },
        },
        {
          type: "input",
          name: "project",
          message: "how you want to call the project",
          validate: (input, answers) => {
            if (input === "") {
              return false;
            }

            const cwd: string = answers.cwd!;

            return !fs.existsSync(npath.join(cwd, input));
          },
        },
      ]);

      const path = npath.join(cwd, project);

      if (!options.noFeatures) {
        for (const feature of template.features) {
          const { name, description } = feature;

          const result = await prompt({
            type: "confirm",
            name: "feature",
            message: `${name}: ${description}`,
          });

          if (result.feature === true) {
            feature.handle(template);
            defaultLogger.debug(`Handled feature ${name}.`);
          }
        }
      } else {
        defaultLogger.debug(
          "skipping features because of '--no-features' flag"
        );
      }

      fs.mkdirSync(path, { recursive: true });

      defaultLogger.debug("installing dependencies");

      await installDependencies(template, options, project, path);

      defaultLogger.debug("initialize file tree");

      for (const file of template.files) {
        if (file.create !== undefined && file.create === false) continue;
        const fpath = npath.join(path, ...file.name.split("/"));
        const parent = fpath.substring(0, fpath.lastIndexOf(npath.sep));
        if (!fs.existsSync(parent)) {
          fs.mkdirSync(parent, { recursive: true });
        }
        fs.writeFileSync(fpath, file.content, { encoding: "utf-8" });
      }

      defaultLogger.info("finished! happy coding :)");
    }
  );

cli.parse();
