/**
 * Interface for defining a file in the template
 */
export interface File {
  /**
   * The path of the file
   */
  name: string;

  /**
   * The content of the file
   */
  content: string;

  /**
   * Determine if the file should be created if the file tree is initialized (optional. default is `true`)
   */
  create?: boolean;
}

/**
 * Interface for defining a feature in the template
 */
export interface Feature {
  /**
   * The name of the feature
   */
  name: string;

  /**
   * The description of the feature that will be printed if the user is asked if he wants to add the feature
   */
  description: string;

  /**
   * Method that handles the feature if the user added the feature to his project
   */
  handle: (template: Template) => void;
}

/**
 * The package manager that should be used
 */
export type PackageManager = "none" | "node";

/**
 * Interface for defining a template
 */
export interface Template {
  /**
   * The files that should be added to the project
   */
  files: File[];

  /**
   * The package manager that should be used in the project
   */
  packageManager: PackageManager;

  /**
   * The dependencies that should be added to the project (optional. default is `[]`)
   */
  dependencies?: string[];

  /**
   * The developer dependencies that should be added to the project (optional. default is `[]`)
   */
  devDependencies?: string[];

  /**
   * Optional features for the user that can be activated on generate
   */
  features: Feature[];
}

function makePackageJson(
  main: string,
  scripts: { [name: string]: string }
): File {
  return {
    name: "package.json",
    content: JSON.stringify(
      {
        name: "<%=PROJECT_NAME=%>",
        version: "1.0.0",
        type: "module",
        license: "MIT",
        main,
        scripts,
      },
      undefined,
      2
    ),
  };
}

function modifyPackageJson(template: Template, value: string, add: any) {
  let files = template.files;
  const file = files.filter((f) => f.name === "package.json")[0];
  if (file === undefined) {
    throw new Error(`Template does not have a package json.`);
  }
  const packageJson = JSON.parse(file.content);
  let field = packageJson[value];
  if (field === null) {
    throw new Error(`PackageJSON has no field called: '${value}'.`);
  }
  field = { ...field, ...add };
  packageJson[value] = field;
  files = files.filter((f) => f.name !== "package.json");
  files.push({
    name: "package.json",
    content: JSON.stringify(packageJson, undefined, 2),
  });
  template.files = files;
}

function modifyFile(
  template: Template,
  name: string,
  handle: (content: string) => string
) {
  let files = template.files;
  const file = files.filter((f) => f.name === name)[0];
  if (file === undefined) {
    throw new Error(`Template does not have a file called '${name}'.`);
  }
  files = files.filter((f) => f.name !== name);
  files.push({
    name: name,
    content: handle(file.content),
  });
  template.files = files;
}

/**
 * Default list of templates used by MCL
 */
export const templates: { [key: string]: Template } = {
  ts: {
    packageManager: "node",
    files: [
      makePackageJson("src/main.ts", {
        start: "node .",
        compile: "tsc",
        dev: "tsc && node .",
      }),
      {
        name: "tsconfig.json",
        content: JSON.stringify(
          {
            compilerOptions: {
              target: "ESNext",
              module: "ES2015",
              declaration: true,
              outDir: "./build",
              rootDir: "./src",
              strict: true,
              esModuleInterop: true,
              moduleResolution: "node",
              skipLibCheck: true,
              forceConsistentCasingInFileNames: true,
            },
          },
          undefined,
          2
        ),
      },
      {
        name: "src/main.ts",
        content: 'console.log("Hallo, Welt!");',
      },
    ],
    dependencies: [],
    devDependencies: ["typescript", "@types/node"],
    features: [
      {
        name: "bundle",
        description:
          "add tsup to your project to bundle the project automatically",
        handle: (template) => {
          template.devDependencies!.push("tsup");
          modifyPackageJson(template, "scripts", {
            pack: "tsup src/main.ts --minify --format esm --target esnext",
          });
        },
      },
      {
        name: "dotenv",
        description: "should be support for .env files added",
        handle: (template) => {
          template.dependencies!.push("dotenv");
          modifyFile(template, "src/main.ts", (content) => {
            return `import("dotenv/config");\n\n${content}`;
          });
          template.files.push({
            name: ".env",
            content: "# add here your enviroment variables",
            create: true,
          });
        },
      },
    ],
  },
};
