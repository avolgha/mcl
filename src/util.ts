import { exec } from "child_process";
import commandExists from "command-exists";
import fs from "fs";
import pkg from "inquirer";
const { prompt } = pkg;
import { colors } from "nstd";
import * as npath from "path";
import { defaultLogger } from "./logger.js";
import { Template } from "./templates.js";

/**
 * @returns The `package.json` file of the current project (MCL)
 */
export function getPackageJson() {
  return JSON.parse(
    fs.readFileSync(npath.join("package.json"), {
      encoding: "utf-8",
    })
  ) as {
    name: string;
    description: string;
    version: string;
  };
}

function cexec(command: string): Promise<void> {
  return new Promise((resolve, reject) => {
    try {
      defaultLogger.debug(`executing command '${command}'.`);
      exec(command, (err, _, __) => {
        if (err) {
          defaultLogger.debug(
            `finished with non-zero exit code ${colors.red("" + err.code)}.`
          );
          reject();
          return;
        }
        defaultLogger.debug(`finished with exit code ${colors.green("0")}.`);
        resolve();
      });
    } catch (err) {
      reject(err);
    }
  });
}

/**
 * Action that is should be performed in `performNodeAction()`
 */
export type NodeAction = "install" | "installDev";

function wrapCwd(command: string, yarn: boolean, cwd?: string) {
  const cmdLabel = yarn ? "yarn" : "npm";
  if (!cwd) return `${cmdLabel} ${command}`;

  if (yarn) {
    return `${cmdLabel} --cwd ${cwd} ${command}`;
  } else {
    if (process.platform === "win32") {
      return `powershell -c "cd ${cwd}; ${cmdLabel} ${command}"`;
    } else {
      return `sh -c "cd ${cwd} && ${cmdLabel} ${command}"`;
    }
  }
}

/**
 * Perform a node action on the specified current working directory
 *
 * @param yarn Determine if yarnpkg should be used
 * @param action The action that should be performed
 * @param data The date you want to parse into the action
 * @param cwd The current working dir
 * @returns Nothing, lol. Only to have a promise for async/await usage
 */
export async function performNodeAction(
  yarn: boolean,
  action: NodeAction,
  data: string[] | string,
  cwd?: string
) {
  let payloadData = Array.isArray(data) ? data.join(" ") : data;

  switch (action) {
    case "install": {
      await cexec(
        wrapCwd(
          yarn ? `add ${payloadData}` : `install --save ${payloadData}`,
          yarn,
          cwd
        )
      );
      return;
    }
    case "installDev": {
      await cexec(
        wrapCwd(
          yarn ? `add -D ${payloadData}` : `install --save-dev ${payloadData}`,
          yarn,
          cwd
        )
      );
      return;
    }
  }
}

/**
 * Determines if yarnpkg should be used as package manager through command flags and user confirmation
 *
 * @param options The options from the generate command in MCL
 * @returns Returns a promise with the information if yarnpkg should be used
 */
export async function checkForYarn(options: {
  useYarn: boolean;
  useNpm: boolean;
}): Promise<boolean> {
  defaultLogger.debug("checking for yarnpkg installation");

  if (options.useYarn === true && !(await commandExists("yarn"))) {
    defaultLogger.error(
      `yarn does not exists on path but the "--use-yarn" flag was set. Please disable the flag or install yarn to use this tool`
    );
    process.exit(0);
  } else if (options.useNpm === true) {
    return false;
  } else {
    if (!(await commandExists("yarn"))) {
      return false;
    } else {
      return (
        await prompt({
          type: "confirm",
          name: "useYarn",
          message: "Should we use yarnpkg to install the dependencies",
        })
      ).useYarn;
    }
  }
}

/**
 * Install dependencies for a template
 *
 * @param template The template you want to create the project with
 * @param options The options from the generate command in MCL
 * @param project The project name
 * @param path The path of the project
 * @returns Nothing, lol. Only to have a promise for async/await usage
 */
export async function installDependencies(
  template: Template,
  options: { useYarn: boolean; useNpm: boolean },
  project: string,
  path: string
) {
  switch (template.packageManager) {
    case "node": {
      const packageJsonFile = template.files.filter(
        (f) => f.name === "package.json"
      )[0];
      if (packageJsonFile === undefined) {
        defaultLogger.error(
          `error in the template: template does not contain a package json file.`
        );
        return;
      }

      const packageJsonContent = packageJsonFile.content.replace(
        "<%=PROJECT_NAME=%>",
        project
      );

      fs.writeFileSync(npath.join(path, "package.json"), packageJsonContent, {
        encoding: "utf-8",
      });

      const yarn = await checkForYarn(options);

      template.dependencies &&
        template.dependencies.length > 0 &&
        (await performNodeAction(yarn, "install", template.dependencies, path));
      template.devDependencies &&
        template.devDependencies.length > 0 &&
        (await performNodeAction(
          yarn,
          "installDev",
          template.devDependencies!,
          path
        ));
      break;
    }
  }
}
