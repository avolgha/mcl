declare module "command-exists" {
  export default function commandExists(commandName: string): Promise<string>;
  export default function commandExists(
    commandName: string,
    callback: (error: any, output: string) => void
  ): void;
  export function sync(commandName: string): boolean;
}
